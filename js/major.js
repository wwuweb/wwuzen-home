(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.majorsScrollspy = {

    attach: function (context) {
      $('body', context).scrollspy({
        target: '#scrollspy-nav'
      });
    }

  };

  Drupal.behaviors.majorsStickyMenu = {

    attach: function (context) {
      enquire.register('screen and (max-width: 819px)', function () {
        $('#majors-sidebar', context).trigger('sticky_kit:detach');
      });

      enquire.register('screen and (min-width: 820px)', function () {
        $('#majors-sidebar', context).stick_in_parent();
      });
    }

  };

}) (jQuery, Drupal, this, this.document);
