(function ($, Drupal, window, document, undefined) {

  /**
   * Toggle the main menu via a button at mobile resolutions.
   */
  Drupal.behaviors.toggleMenu = {

    attach: function (context) {
      var $mainMenu;
      var $toggle;

      // Main menu element
      $mainMenu = $('#main-menu .block-accordion-menu');

      // Main menu mobile toggle button
      $toggle = $('.mobile-main-nav', context);

      // On activation events
      $toggle.on('click keyUp', function () {
        if ($mainMenu.is(':visible')) {
          // Recursively collapse the accordion using the jQuery UI Accordion
          // interface
          $('.accordion-menu-1').accordion('option', 'active', false);

          // Collapse the main menu element
          $mainMenu.slideUp();

          // Reset the main menu style so that it will not be hidden if the
          // browser display is enlarged.
          $mainMenu.promise().done(function () {
            $mainMenu.removeAttr('style');
          });
        } else {
          // Expand the main menu element
          $mainMenu.slideDown();
        }
      });
    }

  };

  /**
   * Insert a "word break opportunity" tag after all occurences of
   * forward-slashes in the page title. This is to accomodate the fixed-width of
   * the majors page sidebar so that extra-long titles do not overflow.
   */
  Drupal.behaviors.breakOpportunity = {

    attach: function (context) {
      // Page heading
      var $heading = $('.page-title h1');

      // Use html() because text() will escape the inserted tag
      $heading.html($heading.text().replace('/', '/<wbr>'));
    }

  };

})(jQuery, Drupal, this, this.document);
