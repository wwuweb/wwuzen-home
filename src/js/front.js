(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.alerts = {

    attach: function (context) {
      // Channel 3: Western Alert
      var emergency = $.getJSON('https://emergency.wwu.edu/api/alert/3');

      // Channel 4: Weather
      var weather = $.getJSON('https://emergency.wwu.edu/api/alert/4');

      function is_all_clear(title) {
        return title.trim().toUpperCase() === 'WWU RSS ALL CLEAR';
      }

      function is_valid_response(data) {
        if (!('Title' in data)) {
          return false;
        }

        if (!('Content' in data)) {
          return false;
        }

        if (!data.Title) {
          return false;
        }

        if (!data.Content) {
          return false;
        }

        return true;
      }

      function log_error(jqXHR, textStatus, errorThrown) {
        console.log(errorThrown);
      }

      function display_alert($alert, data) {
        var $title = $alert.children('.alert-header').find('h2');
        var $content = $alert.children('.alert-content');

        if (!is_all_clear(data.Title)) {
          $title.text(data.Title);
          $content.text(data.Content);
          $alert.slideDown({
            'duration': 1400,
            'easing': 'easeOutElastic'
          });
        }
      }

      emergency.done(function (data) {
        if (is_valid_response(data)) {
          display_alert($('#alert-emergency', context), data);
        }
      });

      weather.done(function (data) {
        if (is_valid_response(data)) {
          display_alert($('#alert-weather', context), data);
        }
      });

      emergency.fail(log_error);
      weather.fail(log_error);
    }

  };

  /**
   * Attach Google Analytics tracking to elements on the home page.
   */
  Drupal.behaviors.ga = {

    attach: function (context) {
      // Admissions action links
      var $admissionsLinks = $('.admissions-clean-homepage a.menu__link', context);
      // Links in the slideshow
      var $heroImageLinks = $('.views-field-field-slideshow-url a, .views-field-field-story-image a', context);
      // Type of hit to track
      var HIT_TYPE = 'event';
      // Type of event to track
      var EVENT_ACTION = 'Click';

      // Check if the analytics.js script has loaded
      if (typeof ga != 'function') {
        return;
      }

      $admissionsLinks.on('click', function (event) {
        event.preventDefault();

        var $target = $(event.target).closest('a');

        var fields = {
          hitType: HIT_TYPE,
          eventCategory: 'Admissions',
          eventAction: EVENT_ACTION,
          eventLabel: $target.text(),
          hitCallback: function () {
            window.location = $target.attr('href');
          }
        };

        ga('send', fields);
        ga('aggregateTracker.send', fields);
      });

      $heroImageLinks.on('click', function (event) {
        event.preventDefault();

        var $target = $(event.target).closest('a');

        ga('send', {
          hitType: HIT_TYPE,
          eventCategory: 'Hero Images',
          eventAction: EVENT_ACTION,
          eventLabel: $target.attr('href'),
          hitCallback: function () {
            window.location = $target.attr('href');
          }
        });
      });
    }

  };

})(jQuery, Drupal, this, this.document);
