// Handle Ajax loading of featured image
// from http://drupal.stackexchange.com/questions/106747/how-to-load-and-display-a-view-with-ajax-in-drupal-7
// Prints a console message with the HTTP GET response code on error
// Druapl's default image will handle missing image errors
(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.loadFeatured = {
    attach: function(context, settings) {
      $url = settings.baseUrl + '/views/ajax';

      $.ajax({
          url: $url, 
          type: 'post',
        data: {
          view_name: 'featured_majors',
          view_display_id: 'default', //your display id
          view_args: {}, // your views arguments
        },
        dataType: 'json',
        success: function (response, status, jqXHR) {
          if (jqXHR.status === 200 && response[1] != undefined) {
           $('.header-wrapper').append(response[1].data).hide().fadeIn(500);
          }
          else {
           var message = "Error: AJAX Request for image failed\nServer responded with " +
              jqXHR.status + "\nRequest URL was " + $url + "\nPlease contact WebTech at webhelp@wwu.edu if you see this message";
           console.log(message);
          }
        }
      });

   }
  };
})(jQuery, Drupal, this, this.document);
