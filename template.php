<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * A QUICK OVERVIEW OF DRUPAL THEMING
 *
 *   The default HTML for all of Drupal's markup is specified by its modules.
 *   For example, the comment.module provides the default HTML markup and CSS
 *   styling that is wrapped around each comment. Fortunately, each piece of
 *   markup can optionally be overridden by the theme.
 *
 *   Drupal deals with each chunk of content using a "theme hook". The raw
 *   content is placed in PHP variables and passed through the theme hook, which
 *   can either be a template file (which you should already be familiary with)
 *   or a theme function. For example, the "comment" theme hook is implemented
 *   with a comment.tpl.php template file, but the "breadcrumb" theme hooks is
 *   implemented with a theme_breadcrumb() theme function. Regardless if the
 *   theme hook uses a template file or theme function, the template or function
 *   does the same kind of work; it takes the PHP variables passed to it and
 *   wraps the raw content with the desired HTML markup.
 *
 *   Most theme hooks are implemented with template files. Theme hooks that use
 *   theme functions do so for performance reasons - theme_field() is faster
 *   than a field.tpl.php - or for legacy reasons - theme_breadcrumb() has "been
 *   that way forever."
 *
 *   The variables used by theme functions or template files come from a handful
 *   of sources:
 *   - the contents of other theme hooks that have already been rendered into
 *     HTML. For example, the HTML from theme_breadcrumb() is put into the
 *     $breadcrumb variable of the page.tpl.php template file.
 *   - raw data provided directly by a module (often pulled from a database)
 *   - a "render element" provided directly by a module. A render element is a
 *     nested PHP array which contains both content and meta data with hints on
 *     how the content should be rendered. If a variable in a template file is a
 *     render element, it needs to be rendered with the render() function and
 *     then printed using:
 *       <?php print render($variable); ?>
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. With this file you can do three things:
 *   - Modify any theme hooks variables or add your own variables, using
 *     preprocess or process functions.
 *   - Override any theme function. That is, replace a module's default theme
 *     function with one you write.
 *   - Call hook_*_alter() functions which allow you to alter various parts of
 *     Drupal's internals, including the render elements in forms. The most
 *     useful of which include hook_form_alter(), hook_form_FORM_ID_alter(),
 *     and hook_page_alter(). See api.drupal.org for more information about
 *     _alter functions.
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   If a theme hook uses a theme function, Drupal will use the default theme
 *   function unless your theme overrides it. To override a theme function, you
 *   have to first find the theme function that generates the output. (The
 *   api.drupal.org website is a good place to find which file contains which
 *   function.) Then you can copy the original function in its entirety and
 *   paste it in this template.php file, changing the prefix from theme_ to
 *   wwuzen_home_. For example:
 *
 *     original, found in modules/field/field.module: theme_field()
 *     theme override, found in template.php: wwuzen_home_field()
 *
 *   where wwuzen is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_field() function.
 *
 *   Note that base themes can also override theme functions. And those
 *   overrides will be used by sub-themes unless the sub-theme chooses to
 *   override again.
 *
 *   Zen core only overrides one theme function. If you wish to override it, you
 *   should first look at how Zen core implements this function:
 *     theme_breadcrumbs()      in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called theme hook suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node--forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and theme hook suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440 and http://drupal.org/node/1089656
 */

/**
 * Override or insert variables into the page templates.
 *
 * @param array $variables
 *   An array of variables to pass to the theme template.
 */
function wwuzen_home_preprocess_page(&$variables) {
  // Path to this theme
  $theme_path = drupal_get_path('theme', 'wwuzen_home');

  // Javascript options
  $js_options = array(
    'group' => JS_THEME,
    'every_page' => FALSE,
  );

  $js_header = array(
    'group' => JS_THEME,
    'every_page' => FALSE,
    'scope' => 'header',
  );

  $js_inline = array(
    'type' => 'inline',
    'group' => JS_THEME,
    'every_page' => FALSE,
    'scope' => 'footer',
  );

  // Render the search box variable so we can place it in the header.
  $search_box = drupal_get_form('thundersearch_form');
  $variables['search_box'] = drupal_render($search_box);

  // Processing for specific page paths
  switch (request_path()) {

    case 'academics':
      drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/c?ppt=523&g=academics&gid=2557&ord="+ordnumber+"&v=113";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt","");document.body.appendChild(x);',
        $js_inline);

      break;

    case 'majors':
      global $base_url;

      drupal_add_js(array('baseUrl' => $base_url), 'setting');
      drupal_add_js($theme_path . '/js/load-featured-majors.min.js', $js_header);
      drupal_add_js($theme_path . '/js/jquery.listnav.min.js', $js_options);
      drupal_add_js($theme_path . '/js/jquery.quicksearch.min.js', $js_options);
      drupal_add_js($theme_path . '/js/majors-landing.min.js', $js_options);
      drupal_add_js("jQuery(function () { jQuery('.accordion-menu-1').accordion('option', 'active', false); });", $js_inline);

      break;

    default:
      if (drupal_is_front_page()) {
        drupal_add_js($theme_path . '/js/front.min.js', $js_options);
        drupal_add_js('var ordnumber = Math.random() * 10000000000000;var sscUrl = ("https:" == document.location.protocol ? "https://" : "http://") + "trkn.us/pixel/c?ppt=523&g=mainpage&gid=2556&ord="+ordnumber+"&v=113";var x = document.createElement("IMG");x.setAttribute("src", sscUrl);x.setAttribute("width", "1");x.setAttribute("height", "1");x.setAttribute("alt","");document.body.appendChild(x);',
          $js_inline);
      }

  }

  // Preprocessing for specific node types
  if (isset($variables['node'])) {
    // Store a local reference to the node
    $node = $variables['node'];

    // Add a page template hook suggestion for each node type
    $variables['theme_hook_suggestions'][] = 'page__' . $node->type;

    switch ($node->type) {

      case 'major':
        // Add javascript required for majors pages functionality
        drupal_add_js($theme_path . '/js/jquery.scrollspy.min.js', $js_options);
        drupal_add_js($theme_path . '/js/jquery.sticky-kit.min.js', $js_options);
        drupal_add_js($theme_path . '/js/major.min.js', $js_options);

        // major title variable
        $field_name = 'field_major_major';
        $items = field_get_items('node', $node, $field_name);
        $variables['major_title'] = field_view_value('node', $node, $field_name,
          $items[0]);

        // major department variable
        $field_name = 'field_major_department';
        $items = field_get_items('node', $node, $field_name);
        // If the major is offered by a department, set the major department
        // variable. Otherwise, fill the major college variable.
        if (!empty($items)) {
          $department = field_view_value('node', $node, $field_name, $items[0]);
          $variables['major_department'] = trim(str_replace('Department', '',
            check_plain($department['#title'])));
        }
        else {
          $field_name = 'field_major_college';
          $items = field_get_items('node', $node, $field_name);
          $college = field_view_value('node', $node, $field_name, $items[0]);
          $variables['major_college'] = check_plain($college['#title']);
        }

        // URI to the current major feature image
        $field_name = 'field_major_image';
        $items = field_get_items('node', $node, $field_name);
        $major_image_uri = file_create_url($items[0]['uri']);

        // URI to the current major node
        $options = array(
          'absolute' => TRUE,
        );
        $major_uri = url('node/' . $node->nid, $options);

        // Twitter social media link
        $options = array(
          'query' => array(
            'status' => 'I am excited to study ' . $node->title . ' at WWU!',
          ),
          'external' => TRUE,
        );
        $variables['major_twitter_link'] = url('https://twitter.com/home', $options);

        // Facebook social media link
        $options = array(
          'query' => array(
            'u' => $major_uri,
          ),
          'external' => TRUE,
        );
        $variables['major_facebook_link'] = url('https://www.facebook.com/sharer.php', $options);

        // Pintrest social media link
        $options = array(
          'query' => array(
            'url' => $major_uri,
            'media' => $major_image_uri,
            'description' => $node->title,
          ),
          'external' => TRUE,
        );
        $variables['major_pintrest_link'] = url('https://www.pinterest.com/pin/create/button', $options);

        // Email socail media link
        $options = array(
          'query' => array(
            'subject' => $node->title . ' at WWU',
            'body' => 'Check out ' . $node->title . ' at WWU by visiting ' . $major_uri,
          ),
          'external' => TRUE,
        );
        $variables['major_email_link'] = url('mailto:', $options);

        break;

    }
  }
  // JS for BrightEdge found in src/js/bright-edge.js
  drupal_add_js('https://cdn.bc0a.com/be_ixf_js_sdk.js', 'external');
  drupal_add_js($theme_path . '/js/bright-edge.min.js', array(
    'type' => 'file',
    'group' => 'JS_THEME',
    'scope' => 'header',
    'every_page' => 'true',
  ));
}

/**
 * Perform necessary alterations to JavaScript be it is presented on the page.
 *
 * Various scripts are excluded from the footer as they are needed globally on
 * the page.
 *
 * @param $javascript
 *   The array of JavaScript files that have been added to the page.
 */
function wwuzen_home_js_alter(&$javascript) {
  foreach ($javascript as &$value) {
    $data = $value['data'];

    if (!is_array($data)) {
      $header = (
        strpos($data, 'ajax') ||
        strpos($data, 'be_ixf_js_sdk') ||
        strpos($data, 'bright-edge') ||
        strpos($data, 'analytics') ||
        strpos($data, 'drupal') ||
        strpos($data, 'imce') ||
        strpos($data, 'jquery') ||
        strpos($data, 'misc')
        // Add additional footer excludes here.
      );
    }
    else {
      $header = FALSE;
    }

    $value['scope'] = ($header) ? 'header' : 'footer';
  }
}

/**
 * Implemements theme_preoprocess_node().
 *
 * Preprocess theme variables before they are sent to the template for
 * rendering. This function allows for content type specific preprocess hooks,
 * which are not part of Drupal core.
 *
 * @param array $variables
 *   The associative array of theme variables
 */
function wwuzen_home_preprocess_node(&$variables) {
  $function = __FUNCTION__ . '_' . $variables['node']->type;

  if (function_exists($function)) {
    $function($variables);
  }
}

/**
 * Implements theme_preprocess_node_HOOK().
 *
 * This is not a core hook; it is enabled by the theme_preprocess_node hook also
 * in this file.
 *
 * @param array $variables
 *   The assiciative array of theme variables
 */
function wwuzen_home_preprocess_node_major(&$variables) {
  // The node that holds the contact information for this major.
  $contact_entity = NULL;

  // Add a variable for easy access to the major title in the template file.
  $variables['major_title'] = NULL;
  $variables['major_college'] = NULL;
  $variables['major_department'] = NULL;
  $variables['preprof_adviser'] = NULL;
  $variables['cip'] = NULL;
  $variables['major_url'] = NULL;
  $variables['major_email'] = NULL;
  $variables['major_phone'] = NULL;

  // Link to the majors index page.
  $variables['browse_all'] = l(t('Browse All Majors'), 'majors');

  if (!empty($variables['field_major_major'])) {
    $field_name = 'field_major_major';
    $variables['major_title'] = field_view_value('node', $variables['node'],
      $field_name, $variables['field_major_major'][0]);
  }

  // Check for the presence of a preprof adviser
  if (!empty($variables['field_pre_professional_adviser'])) {
    $preprof_adviser = $variables['field_pre_professional_adviser'][0]['node'];

    // Set the global to the name of the adviser
    $variables['preprof_adviser'] = check_plain($preprof_adviser->title);

    // Use adviser entity for contact information
    $contact_entity = $preprof_adviser;
  }
  // Repeat the same for centers/institutes/programs
  elseif (!empty($variables['field_center_institute_program'])) {
    $cip = $variables['field_center_institute_program'][0]['node'];

    $variables['cip'] = check_plain($cip->title);

    $contact_entity = $cip;
  }
  elseif (!empty($variables['field_major_department'])) {
    $department = $variables['field_major_department'][0]['node'];

    // Get department name from node reference.
    $variables['major_department'] = trim(str_replace('Department', '', check_plain($department->title)));

    // Use the department reference for contact information.
    $contact_entity = $department;
  }
  elseif (!empty ($variables['field_major_college'])) {
    $college = $variables['field_major_college'][0]['node'];

    // Get college name from node refernece.
    $variables['major_college'] = check_plain($college->title);

    // The department is not set, so use the college reference for contact
    // information instead.
    $contact_entity = $college;
  }

  // Get contact url from node reference.
  $field_name = 'field_ent_url';
  $display = array(
    'type' => 'link_plain',
  );
  $items = field_get_items('node', $contact_entity, $field_name);
  $variables['major_url'] = field_view_value('node', $contact_entity,
    $field_name, $items[0], $display);

  // Get contact email from node reference.
  $field_name = 'field_ent_email';
  $display = array(
    'type' => 'email_plain',
  );
  $items = field_get_items('node', $contact_entity, $field_name);
  $variables['major_email'] = field_view_value('node', $contact_entity,
    $field_name, $items[0], $display);

  // Get contact phone from node reference.
  $field_name = 'field_ent_phone';
  $items = field_get_items('node', $contact_entity, $field_name);
  $variables['major_phone'] = field_view_value('node', $contact_entity,
    $field_name, $items[0]);
}

/**
 * Implememtns theme_preprocess_field().
 *
 * Modify the attributes of individual fields before they are passed to the
 * template for rendering.
 *
 * Use this to set things like the labels for fields in the major content type.
 *
 * @param array $variables
 *   The associative array of variables associated with the field
 */
function wwuzen_home_preprocess_field(&$variables) {
  switch ($variables['element']['#field_name']) {

    case 'field_major_options_select':
      $variables['label'] = 'Explore Related Majors';

      break;

  }
}

/**
 * Implements theme_preprocess_views_vieew_list().
 *
 * Modify the markup of the Views HTML List display. This adds an id attribute
 * to the list for JavaScript usage.
 *
 * @param array $variables
 *   The associative array of variables passed to the template
 */
function wwuzen_home_preprocess_views_view_list(&$variables) {
  // Store reference to view
  $view = $variables['view'];

  // On the majors index view, add an id attribute to the list tag
  if ($view->name == 'majors_index') {
    $find = '>';
    $replace = " id=\"majors-list\">";
    $subject = $variables['list_type_prefix'];
    $variables['list_type_prefix'] = str_replace($find, $replace, $subject);
  }
}
