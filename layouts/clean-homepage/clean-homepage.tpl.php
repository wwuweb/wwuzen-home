<?php
/**
 * @file
 * Template for the new homepage page panel layout.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout.
 */
?>

<div class="right-container-clean-homepage">
  <div class="admissions-clean-homepage" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['admissions']; ?>
  </div>
  <div class="news-events-box">
    <div class="news-clean-homepage" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['news']; ?>
    </div>
    <div class="events-clean-homepage" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['events']; ?>
    </div>
  </div>
</div>

<div class="left-container-clean-homepage">
  <div class="feature-top-clean-homepage" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['feature_top']; ?>
  </div>
  <div class="feature-bottom-clean-homepage" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['feature_bottom']; ?>
  </div>
</div>



