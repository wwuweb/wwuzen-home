<?php

// Plugin definition
$plugin = array(
  'title' => t('clean-homepage'),
  'category' => t('Western'),
  'icon' => 'clean-homepage.png',
  'theme' => 'clean-homepage',
  'css' => '../../css/panel-layouts/clean-homepage.css',
  'regions' => array(
    'admissions' => t('Admissions'),
    'news' => t('News'),
    'events' => t('Events'),
    'feature_top' => t('Feature Top'),
    'feature_bottom' => t('Feature Bottom')
  ),
);
