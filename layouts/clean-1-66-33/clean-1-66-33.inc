<?php

// Plugin definition
$plugin = array(
  'title' => t('clean-1-66-33'),
  'category' => t('Western'),
  'icon' => 'clean-1-66-33.png',
  'theme' => 'clean-1-66-33',
  'css' => '../../css/panel-layouts/clean-1-66-33.css',
  'regions' => array(
    'intro' => t('Intro'),
    'main_left' => t('Main Left'),
    'sidebar_right' => t('Sidebar Right')
  ),
);
