<?php
/**
 * @file
 * Template for the 1-66-33 page panel layout.
 *
 * Provides 1 full-width introduction paragraph and a main content area
 * With a 1/3 right-floated sidebar section
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout.
 */
?>

<div class="container-clean-1-66-33 clean-1-66-33 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\"";} ?>>

  <div class="intro-clean-1-66-33 clean-1-66-33 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['intro']; ?>
  </div>

  <div class="bottom-row-clean-1-66-33 clean-1-66-33 clearfix">
    <div class="main-left-clean-1-66-33 clean-1-66-33 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['main_left']; ?>
    </div>
    <div class="sidebar-right-clean-1-66-33 clean-1-66-33 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['sidebar_right']; ?>
    </div>
  </div>

</div>
