<?php

// Plugin definition
$plugin = array(
  'title' => t('clean-1-1-3-3-3'),
  'category' => t('Western'),
  'icon' => 'clean-1-1-3-3-3.png',
  'theme' => 'clean-1-1-3-3-3',
  'css' => '../../css/panel-layouts/clean-1-1-3-3-3.css',
  'regions' => array(
    'page_banner' => t('Page Banner'),
    'intro' => t('Intro'),
    'left_third' => t('Left Third'),
    'center_third' => t('Center Third'),
    'right_third' => t('Right Third')
  ),
);
