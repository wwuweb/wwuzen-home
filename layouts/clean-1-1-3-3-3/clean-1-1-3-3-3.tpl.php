<?php
/**
 * @file
 * Template for the 50-50 33-33-33 page panel layout.
 *
 * This template provides a top row split into two equal regions and a
 * bottom row split into three equal regions.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout.
 */
?>

<div class="container-clean-1-1-3-3-3 clean-1-1-3-3-3 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>

  <div class="page-banner-clean-1-1-3-3-3 clean-1-1-3-3-3 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['page_banner']; ?>
  </div>
  <div class="intro-clean-1-1-3-3-3 clean-1-1-3-3-3 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['intro']; ?>
  </div>

  <div class="bottom-row-clean-1-1-3-3-3 clean-1-1-3-3-3 clearfix">
    <div class="left-third-clean-1-1-3-3-3 clean-1-1-3-3-3 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['left_third']; ?>
    </div>
    <div class="center-third-clean-1-1-3-3-3 clean-1-1-3-3-3 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['center_third']; ?>
    </div>
    <div class="right-third-clean-1-1-3-3-3 clean-1-1-3-3-3 clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['right_third']; ?>
    </div>
  </div>

</div>
