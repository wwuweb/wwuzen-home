require 'zen-grids'

sass_dir        = "src/sass"
css_dir         = "css"
images_dir      = "images"
javascripts_dir = "js"

relative_assets = true
