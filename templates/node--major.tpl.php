<?php

/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || !$page && $title): ?>
    <header>
      <?php if ($unpublished): ?>
        <mark class="unpublished"><?php print t('Unpublished'); ?></mark>
      <?php endif; ?>
    </header>
  <?php endif; ?>

  <div class="majors-body">

    <header class="majors-header">
      <h2><?php print render($major_title); ?></h2>
    </header>

    <div class="majors-intro" id="majors-intro">
      <?php print render($content['field_major_intro']); ?>
    </div> <!-- end section.majors-intro -->

    <div class="majors-beyond" id="majors-beyond">
      <?php print render($content['field_major_beyond']); ?>
    </div> <!-- end section.majors-beyond -->

    <div class="majors-graduate" id="majors-graduate">
      <?php print render($content['field_major_graduate']); ?>
    </div> <!-- end section.majors-careers -->

    <div class="majors-requirements" id="majors-requirements">
      <?php print render($content['field_requirements']); ?>
    </div>

  </div> <!-- end section.majors-body -->

  <div class="majors-bottom">

    <section class="majors-related">
      <div class="majors-inner-envelope">
        <p class="majors-browse-all"><?php print render($browse_all); ?></p>
        <p class="majors-related"><?php print render($content['field_major_options_select']); ?></p>
        <p class="majors-catalog-link"><?php print render($content['field_catalog_link']); ?></p>
      </div>
    </section> <!-- end section.majors-related -->

    <section class="majors-careers">
      <div class="majors-inner-envelope">
        <?php print render($content['field_major_careers']); ?>
      </div>
    </section> <!-- end section.majors-fotter-center -->

    <section class="majors-contact">
      <div class="majors-inner-envelope">
        <?php if ($major_department): ?>
          <h3>Department of <?php print $major_department; ?></h3>
        <?php elseif($cip): ?>
          <h3><?php print $cip; ?></h3>
        <?php elseif($preprof_adviser): ?>
          <h3>Pre-Professional Adviser</h3>
        <?php else: ?>
          <h3><?php print $major_college; ?></h3>
        <?php endif; ?>
        <ul>
          <li><a class="majors-contact-email" href="mailto:<?php print render($major_email); ?>">contact by email</a></li>
          <li><a class="majors-contact-phone" href="tel:<?php print render($major_phone); ?>"><?php print render($major_phone); ?></a></li>
          <li><a class="majors-contact-website" href="<?php print render($major_url); ?>">department website</a></li>
        </ul>
      </div>
    </section> <!-- end section.majors-contact -->
    <div id="majors-contact" style="clear: both;"></div>
  </div> <!-- end section.majors-bottom -->

  <?php print render($content['links']); ?>

</article> <!-- end article -->
