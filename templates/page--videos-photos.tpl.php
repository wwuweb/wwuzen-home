<?php

/**
 * @file
 * Videos & Photos page template.
 */
?>
<div class="page">

  <header role="banner">

    <section class="western-header" aria-label="University Links, Search, and Navigation">
      <span class="western-logo"><a href="http://www.wwu.edu">Western Washington University</a></span>

      <nav role="navigation" aria-label="University related navigation.">

        <div class="western-quick-links" aria-label="Western Quick Links">
          <button role="button" aria-pressed="false" aria-label="Toggle Quick Links">Toggle Quick Links</button>
          <ul>
            <li><a href="http://www.wwu.edu/academic_calendar" title="Calendar"><span aria-hidden="true">c</span> <span>Calendar</span></a></li>
            <li><a href="http://www.wwu.edu/directory" title="Directory"><span aria-hidden="true">d</span> <span>Directory</span></a></li>
            <li><a href="http://www.wwu.edu/index" title="Index"><span aria-hidden="true">i</span> <span>Index</span></a></li>
            <li><a href="http://www.wwu.edu/campusmaps" title="Map"><span aria-hidden="true">l</span> <span>Map</span></a></li>
            <li><a href="http://mywestern.wwu.edu" title="myWestern"><span aria-hidden="true">w</span> <span>myWestern</span></a></li>
          </ul>
        </div>

        <div class="western-search" role="search" aria-label="University and Site Search">
          <button role="button" aria-pressed="false" aria-label="Open the search box">Open Search</button>

          <div class="western-search-widget">
            <?php print $search_box; ?>
          </div>
        </div>

        <button class="mobile-main-nav" role="button" aria-pressed="false" aria-label="Open Mobile Main Navigation">Open Main Navigation</button>
      </nav>

    </section>
    <nav class="main-nav" id="main-menu" role="navigation" aria-label="Main navigation">
        <?php
          print render($page['navigation']);
        ?>
    </nav>
    <div>
      <section class="site-header" aria-label="Site Header">
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
          <?php if ($logo): ?>
            <div class="site-banner">
              <img src="<?php print $logo;?>" alt="Students walking and bicycling on campus on a sunny day">
              <section class="tagline"><span>Active Minds Changing Lives</span></section>
            </div>
          <?php endif; ?>
        </a>
      </section>

      <?php print render($page['header']); ?>
    </div>

  </header>

  <main role="main">
    <section class="content column">
      <header class="page-title">
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print $breadcrumb; ?>
      </header>
      <?php print render($page['highlighted']); ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
      <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </section>

    <aside class="content-sidebar">
      <ul class="media-links">
        <li class="western-today-media-link"><a href="https://westerntoday.wwu.edu/">WesternToday<br>Campus News &amp; Events</a></li>
        <li class="western-social-media-link"><a href="https://social.wwu.edu/">Western on Social</a></li>
        <li class="western-vimeo-media-link"><a href=" https://vimeo.com/user30954810">Western on Vimeo</a></li>
        <li class="western-youtube-media-link"><a href="https://www.youtube.com/wwu">Western on youTube</a></li>
      </ul>
    </aside>

  </main>

</div> <!-- end div.page -->

  <footer role="contentinfo">
    <div class="footer-wrapper">

      <div class="footer-left">
        <div style="text-align: center;" class="campaign-banner">
          <a href="http://www.foundation.wwu.edu">
            <img alt="Philanthropy makes a difference" src="<?php print $base_path . $directory; ?>/images/philanthropy.png">
          </a>
        </div>
        <?php print render($page['footer_left']); ?>
      </div>

      <div class="footer-center">
        <?php print render($page['footer_center']); ?>
      </div>

      <div class="footer-right" role="complementary" aria-label="WWU contact info">
        <h1><a href="http://www.wwu.edu">Western Washington University</a></h1>

        <div class="western-contact-info">
          <p><span aria-hidden="true" class="western-address"></span>516 High Street<br>
            <span class="western-address-city">Bellingham, WA 98225</span></p>
          <p><span aria-hidden="true" class="western-telephone"></span><a href="tel:3606503000">(360) 650-3000</a></p>
          <p><span aria-hidden="true" class="western-contact"></span><a href="http://www.wwu.edu/wwucontact/">Contact Western</a></p>
        </div>

        <div class="western-social-media">
          <ul>
            <li><a href="https://social.wwu.edu"><span aria-hidden="true" class="westernIcons-WwuSocialIcon"></span>Western Social</a></li>
            <li><a href="http://www.facebook.com/westernwashingtonuniversity"><span aria-hidden="true" class="westernIcons-FacebookIcon"></span>WWU Facebook</a></li>
            <li><a href="http://www.flickr.com/wwu"><span aria-hidden="true" class="westernIcons-FlickrIcon"></span>WWU Flickr</a></li>
            <li><a href="https://twitter.com/WWU"><span aria-hidden="true" class="westernIcons-TwitterIcon"></span>WWU Twitter</a></li>
            <li><a href="http://www.youtube.com/wwu"><span aria-hidden="true" class="westernIcons-YouTubeIcon"></span>WWU Youtube</a></li>
            <li><a href="https://plus.google.com/+wwu/posts"><span aria-hidden="true" class="westernIcons-GooglePlusIcon"></span>WWU Google Plus</a></li>
            <li><a href="http://news.wwu.edu/go/feed/1538/ru/atom/"><span aria-hidden="true" class="westernIcons-RSSicon"></span>Western Today RSS</a></li>
          </ul>
        </div>

        <?php print render($page['footer_right']); ?>
      </div>
    </div> <!-- end div.footer-wrapper -->
  </footer>
<?php print render($page['footer_bottom']); ?>
<?php print render($page['bottom']); ?>
